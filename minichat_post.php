<?php
include('connection.php');
session_start();
if(isset($_POST['pseudo']) AND isset($_POST['message']) AND !empty($_POST['pseudo']) AND !empty($_POST['message'])){
	$_SESSION['pseudo'] = $_POST['pseudo'];
	$req = $bdd->prepare('INSERT INTO minichat (pseudo, message, date) VALUES(?, ?, NOW())');
	$req->execute(array(strip_tags($_POST['pseudo']), strip_tags($_POST['message'])));
}
else{
	$_SESSION['error'] = true;
}

header ('Location: minichat.php ');
?>